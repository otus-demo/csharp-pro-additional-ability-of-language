<Query Kind="Program">
  <Namespace>System.Runtime.InteropServices</Namespace>
</Query>



void Main()
{
	int[] a = new int[5] { 10, 20, 30, 40, 50 };

	// Pin the array in memory using GCHandle
	GCHandle handle = GCHandle.Alloc(a, GCHandleType.Pinned);

	try
	{
		// Get the pointer to the array
		IntPtr pointer = handle.AddrOfPinnedObject();
		Console.WriteLine("Array pinned at memory location: " + pointer);

		// Access elements using pointer (optional)
		unsafe
		{
			int* p = (int*)pointer.ToPointer();
			for (int i = 0; i < a.Length; i++)
			{
				Console.WriteLine("Element {0}: {1}", i, *(p + i));
			}
		}
	}
	finally
	{
		// Free the pinned handle to avoid memory leaks
		handle.Free();
	}
}

// You can define other methods, fields, classes and namespaces here