<Query Kind="Program" />

void Main()
{
	PrintWord();
}

// You can define other methods, fields, classes and namespaces here
private unsafe void PrintWord()
{
	string s = "Привет";

	var sLength = s.Length;

	for (var i = 0; i < sLength; i++)
	{
		Console.WriteLine($"Values is {s[i]} ");
	}

	Console.WriteLine($"----------- ");

	//      2Б  2Б  2Б  2Б  2Б  2Б
	// p -> [П] [р] [и] [в] [е] [т]
	fixed (char* p = s)
	{

		char* p1 = &p[2];
		p1--;
		*p1='f';
		Console.WriteLine($"'{*p1}' - address current {(long)p1} next '{*(p1+1)}' {(long)(p1 + 1)}");

		for (var i = 0; i<sLength+100; i++) //for (var i = 0; p[i]!='\0'; i++) 
		{
			Console.WriteLine($"Values is '{p[i]}' memory address {(long)&p[i]}");
		}

	}
	
	fixed (char* sptr=s){
		 char* st=sptr;
		 *st='r';
		 Console.WriteLine(s);
		 s="Some other string";
		 Console.WriteLine(s);
	}
}