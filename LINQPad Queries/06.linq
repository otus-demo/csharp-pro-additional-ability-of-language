<Query Kind="Program" />

void Main()
{
	ShowSwap();
}

// You can define other methods, fields, classes and namespaces here
unsafe void PSwap(int* a, int* b)
{
	int c = *a;
	*a = *b;
	*b = c;
}


private unsafe void ShowSwap()
{
	int a = 22, b = 3;
	PSwap(&a, &b);
	Console.WriteLine($"a={a} b={b}");

}