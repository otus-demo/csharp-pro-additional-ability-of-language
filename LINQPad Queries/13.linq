<Query Kind="Program">
  <Namespace>System.Windows.Documents</Namespace>
  <Namespace>System.Runtime.InteropServices</Namespace>
  <Namespace>System.Runtime.CompilerServices</Namespace>
</Query>

void Main()
{
	string[] people = new string[] { "Tom", "Alice", "Bob","Mike" };
	Span<string> peopleSpan = new Span<string>(people);
	Console.WriteLine(peopleSpan.ToArray());
	Reverse<string>(peopleSpan);
	Console.WriteLine(peopleSpan.ToArray());
	UnsafeReverse<string>(peopleSpan);
	Console.WriteLine(peopleSpan.ToArray());
}

static void Reverse<T>(Span<T> span)
{
	while (span.Length > 1)
	{
		T firstElement = span[0];
		T lastElement = span[^1];
		span[0] = lastElement;
		span[^1] = firstElement;
		span = span[1..^1];
	}
}

// A correct but unverifiable way to reverse a Span<T>.
static void UnsafeReverse<T>(Span<T> span)
{
	if (span.Length > 1)
	{
		ref T refLeft = ref MemoryMarshal.GetReference(span);
		ref T refRight = ref Unsafe.Add(ref refLeft, span.Length - 1);
		do
		{
			T leftElement = refLeft;
			T rightElement = refRight;
			refLeft = rightElement;
			refRight = leftElement;
			refLeft = ref Unsafe.Add(ref refLeft, 1);
			refRight = ref Unsafe.Subtract(ref refRight, 1);
		} while (Unsafe.IsAddressLessThan(ref refLeft, ref refRight));
	}
}