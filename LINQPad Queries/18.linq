<Query Kind="Program" />

unsafe void Main()
{
	string str = "Hello";  // Immutable
	char[] charArray = new char[] { 'H', 'e', 'l', 'l', 'o' };  // Mutable
	
	unsafe
	{
		// Pinning a string (Read-only access)
		fixed (void* strPtr = str)
		{
			char i=str[1];
			Console.WriteLine(i);
			char* ch0=(char*)strPtr;
			Console.WriteLine("First char in string: " + *ch0);
			 *ch0 = 'X';  // Error: Cannot modify string because it's immutable
			Console.WriteLine(str);
			Console.WriteLine(i);
		}
	
		// Pinning a char array (Read/Write access)
		fixed (char* charArrayPtr = &charArray[2])
		{
			Console.WriteLine("First char in charArray: " + *charArrayPtr);
			*charArrayPtr = 'X';  // Modifies the first element in the char array
			Console.WriteLine("Modified charArray: " + new string(charArray));
		}
	}
}

// You can define other methods, fields, classes and namespaces here