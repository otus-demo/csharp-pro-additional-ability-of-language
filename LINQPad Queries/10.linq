<Query Kind="Program" />

void Main()
{
	new TrashDispenser().DispenseV2(new Can());
	
	new TrashDispenser().Dispense(new Paper());
}

// You can define other methods, fields, classes and namespaces here

public class TrashItem { }

public class Can : TrashItem { }

public class Paper : TrashItem { }

/// <summary>
/// Переработчик мусора
/// </summary>
public class TrashDispenser
{

	/// <summary>
	/// Переработчик классический
	/// </summary>
	/// <param name="ti"></param>
	public void DispenseV2(TrashItem ti)
	{
		if (ti is Can)
		{
			DispensePlease((Can)ti);
		}
		else if (ti is Paper)
		{
			DispensePlease((Paper)ti);
		}
	}


	/// <summary>
	/// Переработчик с dynamic
	/// </summary>
	/// <param name="item"></param>
	public void Dispense(TrashItem item)
	{
		DispensePlease(item as dynamic);
	}


	private void DispensePlease(Can can)
	=> Console.WriteLine("Dispancing can");

	private void DispensePlease(Paper paper)
	=> Console.WriteLine("Dispancing paper");
}